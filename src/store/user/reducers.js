import { FETCH_ACTIVE_USER, LOGIN_USER } from "./constants";
import userState from "./state";

const userReducers = (state = userState, action) => {
  switch (action.type) {
    case FETCH_ACTIVE_USER:
      return {
        ...state,
        user: action.payload.data,
      };
    case LOGIN_USER:
      const user = action.payload.data;
      localStorage.setItem("userToken", user.token);
      localStorage.setItem("userId", user.id);
      return {
        ...state,
        user,
      };
    default:
      return state;
  }
};

export default userReducers;
