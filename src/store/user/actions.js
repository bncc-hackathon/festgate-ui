import api from "api/controllers/user";
import { FETCH_ACTIVE_USER, LOGIN_USER } from "./constants";

export const fetchActiveUser = () => async (dispatch) => {
  const { data } = await api.getActiveUser();
  dispatch({
    type: FETCH_ACTIVE_USER,
    payload: data,
  });
};

export const login = (request) => async (dispatch) => {
  const { data } = await api.login(request);
  dispatch({
    type: LOGIN_USER,
    payload: data,
  });
};

export const register = (request, { success, failed }) => async (dispatch) => {
  try {
    const { data } = await api.uploadToS3(request.image);
    request.image_path = data.data;
    await api.register(request);
    success();
  } catch (e) {
    failed(e);
  }
};

export const verify = (request, { success, failed }) => async (dispatch) => {
  try {
    // await api.verify(request);
    success();
  } catch (e) {
    failed(e);
  }
};
