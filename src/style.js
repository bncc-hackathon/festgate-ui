import { createGlobalStyle } from "styled-components";

import Lato from "assets/fonts/Lato/Lato-Regular.ttf";
import Quicksand from "assets/fonts/Quicksand/Quicksand-Regular.ttf";

export const GlobalStyleComponent = createGlobalStyle`
  @font-face {
    font-family: Lato;
    src: url(${Lato});
  }
  @font-face {
    font-family: Quicksand;
    src: url(${Quicksand});
  }
  html, body {
    margin: 0px;
    padding: 0px;
  }
`;
