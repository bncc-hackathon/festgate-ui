export default {
  getActiveUser: (id) => `/api/user/${id}`,
  login: "/api/user/login",
  register: "/api/user/register",
  uploadToS3: "/api/register",
  compareFace: (id) => `/api/verify/${id}`,
};
