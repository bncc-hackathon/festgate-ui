import instance from "api";

export default {
  getRequest(path, params, headers) {
    return instance.get(path, { params, headers });
  },
  postRequest(path, body, headers) {
    return instance.post(path, body, { headers });
  },
  patchRequest(path, body) {
    return instance.patch(path, body);
  },
  deleteRequest(path) {
    return instance.delete(path);
  },
};
