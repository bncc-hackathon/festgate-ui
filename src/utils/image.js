const base64ToBlob = async (base64) =>
  await fetch(base64).then((res) => res.blob());

const blobToFile = (blob) => {
  blob.lastModifiedDate = new Date();
  blob.name = blob.lastModifiedDate;
  return blob;
};

export default {
  base64ToBlob,
  blobToFile,
};
