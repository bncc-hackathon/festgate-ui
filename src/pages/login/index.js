import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";

import { login } from "store/user/actions";

import Navbar from "components/common/navbar";

import Logo from "assets/images/Logo.png";

import {
  IconLogo,
  LoginHeadline,
  LoginPageContainer,
  LoginPageForm,
  SignInReminderLabel,
  RegisterLink,
  InputTextField,
  LoginButton,
  ErrorLabel,
  RememberMeCheckBox,
  RememberMeGroupBox,
} from "./style";

const FORM_LIST = {
  email: {
    placeholder: "Email",
    value: "",
    error: "",
  },
  password: {
    placeholder: "Password",
    value: "",
    error: "",
  },
};

const Login = () => {
  const [form, setForm] = useState(FORM_LIST);
  const user = useSelector(({ userReducers }) => userReducers.user);
  const dispatch = useDispatch();
  const history = useHistory();

  user && user.id && history.push("/");

  const handleChange = (e) => {
    let updatedForm = { ...form };
    updatedForm[e.target.name].value = e.target.value;
    updatedForm[e.target.name].error = "";
    setForm(updatedForm);
  };

  const handleSubmit = (e) => {
    const isValid = isValidateForm();
    if (isValid) {
      dispatch(
        login({
          email: form.email.value,
          password: form.password.value,
        })
      );
    }
  };

  const isValidateForm = () => {
    let isValid = true;
    let updatedForm = { ...form };
    Object.keys(FORM_LIST).forEach((input) => {
      if (form[input].value === "") {
        updatedForm[
          input
        ].error = `${FORM_LIST[input].placeholder} can't be null`;
        setForm(updatedForm);
        isValid = false;
      }
    });
    return isValid;
  };

  const renderInputs = () =>
    Object.keys(FORM_LIST).map((input) => (
      <>
        <InputTextField
          key={input}
          onChange={handleChange}
          name={input}
          type={
            input === "password" || input === "confirmPassword"
              ? "password"
              : "text"
          }
          label={FORM_LIST[input].placeholder}
          variant="outlined"
        />
        {form[input].error && (
          <ErrorLabel key={form[input].error}>{form[input].error}</ErrorLabel>
        )}
      </>
    ));

  return (
    <>
      <Navbar />
      <LoginPageContainer>
        <IconLogo width="140px" src={Logo} />
        <LoginHeadline>Make an account for free.</LoginHeadline>
        <LoginPageForm>
          {renderInputs()}
          <LoginButton onClick={handleSubmit} type="click">
            Login
          </LoginButton>
          <RememberMeGroupBox
            control={<RememberMeCheckBox name="remember" />}
            label="Remember Me"
          />
          <SignInReminderLabel>
            Don't have an account ?
            <Link to="/register" style={{ textDecoration: "none" }}>
              <RegisterLink>Register Now</RegisterLink>
            </Link>
          </SignInReminderLabel>
        </LoginPageForm>
      </LoginPageContainer>
    </>
  );
};

export default Login;
