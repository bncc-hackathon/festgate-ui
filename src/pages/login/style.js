import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import {
  TextField,
  Button,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import {
  COLOR_MAIN,
  COLOR_TEXT_MAIN,
  COLOR_TEXT_SECONDARY,
  COLOR_TEXT_DANGER,
} from "constants/color";

import Background from "assets/images/auth.png";

export const IconLogo = styled.img``;

export const LoginHeadline = styled.label`
  font-family: Lato;
  margin: 10px 0;
`;

export const LoginPageContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  height: 87vh;
  justify-content: flex-start;
  padding-top: 100px;
  background-image: url(${Background});
  background-size: cover;
  background-position: bottom;
`;
export const LoginPageForm = styled.div`
  border-radius: 10px;
  width: 20%;
  padding: 36px 25px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  background: rgba(255, 255, 255, 1);
  box-shadow: 0px 4px 10px 4px rgba(105, 105, 105, 0.06);
  @media (max-width: 1440px) {
    width: 50%;
  }
  @media (max-width: 1024px) {
    width: 70%;
  }
`;

export const SignInReminderLabel = styled.label`
  font-family: Lato;
  text-align: center;
`;

export const RegisterLink = styled.label`
  color: ${COLOR_MAIN};
  font-weight: bolder;
  cursor: pointer;
  margin-left: 5px;
`;

export const ErrorLabel = styled.label`
  font-family: Lato
  color: ${COLOR_TEXT_DANGER};
`;

export const InputTextField = withStyles({
  root: {
    margin: "12px 0px",
  },
})(TextField);

export const LoginButton = withStyles({
  root: {
    margin: "12px 0px",
    backgroundColor: COLOR_TEXT_MAIN,
    color: COLOR_TEXT_SECONDARY,
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
      fontWeight: "bolder",
      color: COLOR_TEXT_SECONDARY,
    },
  },
})(Button);

export const RememberMeGroupBox = withStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(FormControlLabel);

export const RememberMeCheckBox = withStyles({
  root: {
    color: `${COLOR_MAIN} important`,
    "&:checked": {
      color: COLOR_MAIN,
      backgroundColor: COLOR_MAIN,
    },
  },
})(Checkbox);
