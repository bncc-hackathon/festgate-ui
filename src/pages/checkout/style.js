import styled from "styled-components";

import BG from "assets/images/checkout_bg.svg";
import { COLOR_MAIN } from "constants/color";

export const CheckoutPageContainer = styled.div`
  font-family: Lato;
  display: flex;
  height: 90vh;
  align-items: center;
  flex-direction: column;
  background-image: url(${BG});
  background-size: cover;
`;

export const BackLabelContainer = styled.div`
  margin: 10px;
  display: flex;
  align-items: center;
`;

export const BackLabel = styled.label`
  color: ${COLOR_MAIN}
  cursor: pointer;
  margin-left: 10px;
  text-align: left;
  font-weight: bolder;
`;
