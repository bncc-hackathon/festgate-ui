import React, { useState } from "react";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import Navbar from "components/common/navbar";
import PageNumberSection from "components/pages/checkout/page-number-section";
import CheckoutCardSection from "components/pages/checkout/checkout-card-section";

import { CheckoutPageContainer, BackLabel, BackLabelContainer } from "./style";
import SuccessSection from "components/pages/checkout/success-section";

const MESSAGES = [
  "",
  "Back to Checkout",
  "Back to Checkout",
  "Back to Payment Method",
];

const Checkout = () => {
  const [currentPage, setCurrentPage] = useState(1);

  const handleBack = () => {
    let nexPage = currentPage;
    setCurrentPage(nexPage - 1);
  };

  return (
    <>
      <Navbar />
      <CheckoutPageContainer>
        {currentPage !== 5 ? (
          <>
            <PageNumberSection currentPage={currentPage} />
            <CheckoutCardSection
              currentPage={currentPage}
              setCurrentPage={setCurrentPage}
            />
            {currentPage !== 1 && (
              <BackLabelContainer>
                <ArrowBackIcon
                  style={{ color: "#3F5EFB" }}
                  onClick={handleBack}
                />
                <BackLabel onClick={handleBack}>
                  {MESSAGES[currentPage - 1]}
                </BackLabel>
              </BackLabelContainer>
            )}
          </>
        ) : (
          <SuccessSection />
        )}
      </CheckoutPageContainer>
    </>
  );
};
export default Checkout;
