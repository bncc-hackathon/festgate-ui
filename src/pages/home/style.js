import styled from "styled-components";
import Background from "assets/images/background.svg";

export const HomePageContainer = styled.div``;

export const BackgroundWavyWrapper = styled.div`
  background-image: url(${Background});
  background-size: cover;
  @media (max-width: 1440px) {
    background-position: right;
  }
`;
