import React from "react";

import Navbar from "components/common/navbar";
import HeadlineSection from "components/pages/home/headline-section";
import UpcomingSection from "components/pages/home/upcoming-section";
import PopularSection from "components/pages/home/popular-section";
import TrendingSection from "components/pages/home/trending-section";

import { HomePageContainer, BackgroundWavyWrapper } from "./style";

const HomePage = () => {
  return (
    <>
      <Navbar />
      <HomePageContainer>
        <HeadlineSection />
        <UpcomingSection />
        <BackgroundWavyWrapper>
          <PopularSection />
          <TrendingSection />
        </BackgroundWavyWrapper>
      </HomePageContainer>
    </>
  );
};

export default HomePage;
