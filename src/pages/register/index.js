import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import Logo from "assets/images/Logo.png";

import { IconLogo, RegisterHeadline, RegisterPageContainer } from "./style";

import Navbar from "components/common/navbar";
import RegisterPageSection from "components/pages/register/register-page-section";

const Register = () => {
  const user = useSelector(({ userReducers }) => userReducers.user);
  const history = useHistory();

  user && user.id && history.push("/");

  return (
    <>
      <Navbar />
      <RegisterPageContainer>
        <IconLogo width="140px" src={Logo} />
        <RegisterHeadline>Make an account for free.</RegisterHeadline>
        <RegisterPageSection />
      </RegisterPageContainer>
    </>
  );
};

export default Register;
