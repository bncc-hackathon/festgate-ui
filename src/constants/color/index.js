export const COLOR_MAIN = "#3F5EFB";
export const COLOR_SECONDARY = "#FC466B";
export const COLOR_TEXT_MAIN = "#4B505E";
export const COLOR_TEXT_SECONDARY = "#C4C4C4";
export const COLOR_TEXT_DANGER = "#FF4545";
