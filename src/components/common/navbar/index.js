import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import RadioButtonUncheckedOutlinedIcon from "@material-ui/icons/RadioButtonUncheckedOutlined";

import {
  NavbarContainer,
  FestageLogo,
  SignInButton,
  ConcertTextField,
  ExtraSearchContainer,
  UserLabel,
  UserGroup,
} from "./style";

import Search from "@material-ui/icons/Search";
import InputAdornment from "@material-ui/core/InputAdornment";
import LogoLabel from "assets/images/LogoLabel.svg";

const Navbar = () => {
  const user = useSelector(({ userReducers }) => userReducers.user);
  const history = useHistory();

  return (
    <NavbarContainer>
      <FestageLogo onClick={() => history.push("/")} src={LogoLabel} />
      {history.location.pathname === "/" && (
        <ExtraSearchContainer>
          <ConcertTextField
            placeholder="Search for artists, venues, and events"
            InputProps={{
              disableUnderline: true,
              startAdornment: (
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
        </ExtraSearchContainer>
      )}
      {user && user.id ? (
        <UserGroup>
          <RadioButtonUncheckedOutlinedIcon
            style={{ color: "#3F5EFB", fontSize: "3em" }}
          />
          <UserLabel>{user.name}</UserLabel>
        </UserGroup>
      ) : (
        <SignInButton onClick={() => history.push("login")}>
          Sign In
        </SignInButton>
      )}
    </NavbarContainer>
  );
};

export default Navbar;
