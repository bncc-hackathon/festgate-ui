import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button, TextField } from "@material-ui/core";

import { COLOR_MAIN } from "constants/color";

export const FestageLogo = styled.img`
  width: 100px;
  cursor: pointer;
`;

export const NavbarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 40px;
`;

export const ExtraSearchContainer = styled.div`
  width: 70%;
  display: flex;
  margin: 10px 0px;
  justify-content: center;
`;

export const UserLabel = styled.h2`
  font-family: Lato;
  color: ${COLOR_MAIN};
  margin-left: 10px;
`;

export const UserGroup = styled.div`
  display: flex;
  align-items: center;
`;

export const ConcertTextField = withStyles({
  root: {
    fontFamily: "Lato",
    border: "solid",
    borderRadius: "10px",
    outline: "none",
    textDecoration: "none",
    width: "80%",
    borderWidth: "0.2px",
    padding: "4px",
    "@media (max-width: 768px)": {
      display: "none",
    },
  },
})(TextField);

export const SignInButton = withStyles({
  root: {
    fontFamily: "Quicksand",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_MAIN,
    color: "white",
    borderRadius: "25px",
    padding: "6px 40px",
    fontWeight: "bolder",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
    "@media (max-width: 768px)": {
      padding: "8px 20px",
    },
  },
})(Button);
