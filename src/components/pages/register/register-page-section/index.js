import React, { useState } from "react";
import CameraSection from "components/pages/register/camera-section";
import RegisterFormSection from "components/pages/register/register-form-section";
import FaceRegistryDetailSection from "components/pages/register/face-registry-detail-section";
import SuccessSection from "components/pages/register/success-section";

import {
  RegisterPageSectionContainer,
  CarouselGroup,
  CarouselItem,
} from "./style";

const FORM_LIST = {
  name: {
    placeholder: "Full Name",
    value: "",
    error: "",
  },
  email: {
    placeholder: "Email",
    value: "",
    error: "",
  },
  password: {
    placeholder: "Password",
    value: "",
    error: "",
  },
  confirmPassword: {
    placeholder: "Confirm Password",
    value: "",
    error: "",
  },
};

const RegisterPageSection = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [form, setForm] = useState(FORM_LIST);
  return (
    <RegisterPageSectionContainer currentPage={currentPage}>
      <CarouselGroup>
        {currentPage < 3 && (
          <>
            <CarouselItem page={1} currentPage={currentPage}>
              <RegisterFormSection
                form={form}
                setForm={setForm}
                handleChangePage={setCurrentPage}
              />
            </CarouselItem>
            <CarouselItem>
              <FaceRegistryDetailSection
                handleChangePage={setCurrentPage}
                currentPage={currentPage}
              />
            </CarouselItem>
          </>
        )}
        {currentPage === 3 && (
          <CarouselItem>
            <CameraSection
              handleChangePage={setCurrentPage}
              currentPage={currentPage}
              form={form}
            />
          </CarouselItem>
        )}

        {currentPage === 4 && (
          <CarouselItem>
            <SuccessSection />
          </CarouselItem>
        )}
      </CarouselGroup>
    </RegisterPageSectionContainer>
  );
};

export default RegisterPageSection;
