import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { COLOR_MAIN } from "constants/color";

export const FaceRegistryDetailContainer = styled.div`
  padding: 20px;
`;
export const AddFaceVerificationLabel = styled.h2``;
export const AddFaceVerificationDescription = styled.label``;

export const SkipLabel = styled.h6`
  margin: 0;
  color: ${COLOR_MAIN};
  text-align: center;
  cursor: pointer:
`;

export const AddFaceVerificationButton = withStyles({
  root: {
    margin: "40px 0px 20px 0px",
    backgroundColor: COLOR_MAIN,
    color: "white",
    borderRadius: "25px",
    textTransform: "none",
    fontWeight: "bolder",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(Button);
