import React from "react";
import {
  AddFaceVerificationLabel,
  FaceRegistryDetailContainer,
  AddFaceVerificationDescription,
  AddFaceVerificationButton,
  SkipLabel,
} from "./style";

const FaceRegistryDetailSection = ({ handleChangePage }) => {
  return (
    <FaceRegistryDetailContainer>
      <AddFaceVerificationLabel>Add Face Verification</AddFaceVerificationLabel>
      <AddFaceVerificationDescription>
        Face verification helps us add more protection to your account. By
        scanning your face, we will:
        <ul>
          <li>Save your details for future checkout</li>
          <li>Provide a faster checkout process</li>
          <li>
            Ensure your details are safe Do you want to add face verification?
          </li>
        </ul>
      </AddFaceVerificationDescription>
      <AddFaceVerificationButton onClick={() => handleChangePage(3)}>
        Yes, add face verification
      </AddFaceVerificationButton>
      <SkipLabel>Skip For Now</SkipLabel>
    </FaceRegistryDetailContainer>
  );
};

export default FaceRegistryDetailSection;
