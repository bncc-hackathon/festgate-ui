import React, { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import Webcam from "react-webcam";

import { register } from "store/user/actions";
import imageUtil from "utils/image";

import Frame from "assets/images/frame.svg";

import {
  CameraSectionContainer,
  ActionButton,
  FocusFrame,
  ButtonGroup,
  FrameContainer,
  BackButton,
} from "./style";

const CameraSection = ({ currentPage, handleChangePage, form }) => {
  const cameraSectionContainerRef = useRef(null);
  const dispatch = useDispatch();
  const webcamRef = React.useRef(null);
  const [error, setError] = useState(null);

  const handleSubmit = React.useCallback(
    (imageBase64) => {
      setError(null);
      const promise = imageUtil.base64ToBlob(imageBase64);
      promise.then((res) => {
        const image = imageUtil.blobToFile(res);
        const request = {
          name: form.name.value,
          email: form.email.value,
          password: form.password.value,
          image,
        };
        dispatch(
          register(request, {
            success: () => handleChangePage(4),
            failed: () => setError("Face not Detected"),
          })
        );
      });
    },
    [dispatch, form, handleChangePage]
  );

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    handleSubmit(imageSrc);
  }, [webcamRef, handleSubmit]);

  return (
    <CameraSectionContainer ref={cameraSectionContainerRef}>
      {currentPage === 3 ? (
        <>
          <Webcam
            audio={false}
            ref={webcamRef}
            screenshotFormat="image/jpeg"
            style={{ width: "100%" }}
          />
          <FrameContainer>
            <FocusFrame
              currentPage={currentPage}
              containerRef={cameraSectionContainerRef}
              src={Frame}
            />
            <label
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                color: "#FC466B",
              }}
            >
              {error}
            </label>
          </FrameContainer>
          <ButtonGroup>
            <BackButton onClick={() => handleChangePage(2)}>Back</BackButton>
            <ActionButton onClick={capture}>Confirm</ActionButton>
          </ButtonGroup>
        </>
      ) : (
        <div></div>
      )}
    </CameraSectionContainer>
  );
};

export default CameraSection;
