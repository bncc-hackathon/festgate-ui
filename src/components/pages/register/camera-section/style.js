import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { COLOR_MAIN } from "constants/color";

export const CameraSectionContainer = styled.div`
  padding: 20px;
  position: relative;
`;

export const FrameContainer = styled.div`
  position: absolute;
  left: 200px;
  top: 80px;
  @media (max-width: 1280px) {
    left: 180px;
  }
`;

export const FocusFrame = styled.img`
  width: 200px;
  height: 240px;
  @media (max-width: 1280px) {
    width: ${(props) => {
      return props.currentPage !== 4 ? 350 : 120;
    }}px;
  }
`;

export const ButtonGroup = styled.div`
  position: absolute;
  bottom: 50px;
  left: 0;
  width: 100%;
  display: flex;
  justify-content: space-around;
`;

export const BackButton = withStyles({
  root: {
    backgroundColor: "white",
    color: COLOR_MAIN,
    borderRadius: "25px",
    textTransform: "none",
    fontWeight: "bolder",
    width: "20%",
    padding: "10px 40px",
    "&:hover": {
      backgroundColor: "white",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(Button);

export const ActionButton = withStyles({
  root: {
    backgroundColor: COLOR_MAIN,
    color: "white",
    borderRadius: "25px",
    textTransform: "none",
    fontWeight: "bolder",
    width: "20%",
    padding: "10px 40px",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(Button);
