import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { TextField, Button } from "@material-ui/core";
import {
  COLOR_MAIN,
  COLOR_TEXT_MAIN,
  COLOR_TEXT_SECONDARY,
  COLOR_TEXT_DANGER,
} from "constants/color";

export const RegisterPageForm = styled.div`
  border-radius: 10px;
  padding: 36px 25px;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const SignInReminderLabel = styled.label`
  font-family: Lato;
  text-align: center;
`;

export const SignInLink = styled.label`
  color: ${COLOR_MAIN};
  font-weight: bolder;
  cursor: pointer;
  margin-left: 5px;
`;

export const ErrorLabel = styled.label`
  font-family: Lato
  color: ${COLOR_TEXT_DANGER};
`;

export const InputTextField = withStyles({
  root: {
    margin: "12px 0px",
  },
})(TextField);

export const RegisterButton = withStyles({
  root: {
    margin: "12px 0px",
    backgroundColor: COLOR_TEXT_MAIN,
    color: COLOR_TEXT_SECONDARY,
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
      fontWeight: "bolder",
      color: COLOR_TEXT_SECONDARY,
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(Button);
