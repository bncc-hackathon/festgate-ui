import React from "react";
import { Link } from "react-router-dom";

import {
  RegisterPageForm,
  SignInReminderLabel,
  SignInLink,
  InputTextField,
  RegisterButton,
  ErrorLabel,
} from "./style";

const FORM_LIST = {
  name: {
    placeholder: "Full Name",
    value: "",
    error: "",
  },
  email: {
    placeholder: "Email",
    value: "",
    error: "",
  },
  password: {
    placeholder: "Password",
    value: "",
    error: "",
  },
  confirmPassword: {
    placeholder: "Confirm Password",
    value: "",
    error: "",
  },
};

const RegisterFormSection = ({ form, setForm, handleChangePage }) => {
  const handleChange = (e) => {
    let updatedForm = { ...form };
    updatedForm[e.target.name].value = e.target.value;
    updatedForm[e.target.name].error = "";
    setForm(updatedForm);
  };

  const handleSubmit = () => {
    const isValid = isValidateForm();
    if (isValid) {
      handleChangePage(2);
    }
  };

  const isValidateForm = () => {
    let isValid = true;
    let updatedForm = { ...form };
    Object.keys(FORM_LIST).forEach((input) => {
      if (form[input].value === "") {
        updatedForm[
          input
        ].error = `${FORM_LIST[input].placeholder} can't be null`;
        setForm(updatedForm);
        isValid = false;
      }
    });
    if (form.password.value !== form.confirmPassword.value) {
      updatedForm.confirmPassword.error = `Password is not match`;
      setForm(updatedForm);
      isValid = false;
    }
    return isValid;
  };

  const renderInputs = () =>
    Object.keys(FORM_LIST).map((input) => (
      <React.Fragment key={"register_" + input}>
        <InputTextField
          onChange={handleChange}
          name={input}
          type={
            input === "password" || input === "confirmPassword"
              ? "password"
              : "text"
          }
          label={FORM_LIST[input].placeholder}
          variant="outlined"
        />
        {form[input].error && <ErrorLabel>{form[input].error}</ErrorLabel>}
      </React.Fragment>
    ));

  return (
    <RegisterPageForm>
      {renderInputs()}
      <RegisterButton onClick={handleSubmit} type="click">
        Next
      </RegisterButton>
      <SignInReminderLabel>
        Already have an account?
        <Link to="/login" style={{ textDecoration: "none" }}>
          <SignInLink>Sign In</SignInLink>
        </Link>
      </SignInReminderLabel>
    </RegisterPageForm>
  );
};

export default RegisterFormSection;
