import React from "react";
import Okay from "assets/images/okay.png";
import {
  SuccessSectionContainer,
  RegistrationCompleteIcon,
  RegistrationCompleteDescription,
  RegistrationCompleteLabel,
  RegistrationCompleteTextGroup,
  FindConcertButton,
} from "./style";
import { useHistory } from "react-router-dom";

const SuccessSection = () => {
  const history = useHistory();
  return (
    <SuccessSectionContainer>
      <RegistrationCompleteIcon src={Okay} />
      <RegistrationCompleteTextGroup>
        <RegistrationCompleteLabel>
          Registration Completed!
        </RegistrationCompleteLabel>
        <RegistrationCompleteDescription>
          Your profile has been saved. Thank you for joining us!
        </RegistrationCompleteDescription>
      </RegistrationCompleteTextGroup>
      <FindConcertButton onClick={() => history.push("/login")}>
        Find some concerts
      </FindConcertButton>
    </SuccessSectionContainer>
  );
};

export default SuccessSection;
