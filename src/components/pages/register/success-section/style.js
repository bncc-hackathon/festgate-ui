import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { COLOR_MAIN } from "constants/color";

export const SuccessSectionContainer = styled.div`
  display: flex;
  padding: 20px;
  flex-direction: column;
  align-items: center;
`;
export const RegistrationCompleteIcon = styled.img`
  width: 150px
  height: 150px;
`;

export const RegistrationCompleteTextGroup = styled.div`
  text-align: center;
`;

export const RegistrationCompleteLabel = styled.h2``;
export const RegistrationCompleteDescription = styled.h4``;

export const FindConcertButton = withStyles({
  root: {
    margin: "40px 0px 20px 0px",
    backgroundColor: COLOR_MAIN,
    color: "white",
    borderRadius: "25px",
    textTransform: "none",
    fontWeight: "bolder",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(Button);
