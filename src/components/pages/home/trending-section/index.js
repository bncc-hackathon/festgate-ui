import React from "react";

import ArrowSvg from "assets/images/Arrow.svg";

import {
  TrendingSectionContainer,
  TrendingSectionHeadline,
  ScrollTrendingArtistContainer,
  TrendingArtistCardGroup,
  TrendingArtistCard,
  TrendingArtistName,
  SeeMoreTrendingGroup,
  SeeMoreTrendingLabel,
  SeeAllTrendingArtistButton,
} from "./style";

const NAMES = ["BTS", "BLACKPINK", "TWICE", "YOASOBI"];

const renderPopulars = () =>
  NAMES.map((name) => (
    <TrendingArtistCard key={name}>
      <TrendingArtistName>{name}</TrendingArtistName>
    </TrendingArtistCard>
  ));

const TrendingSection = () => (
  <TrendingSectionContainer>
    <TrendingSectionHeadline>Top Trending Artists</TrendingSectionHeadline>
    <ScrollTrendingArtistContainer>
      <TrendingArtistCardGroup>
        {renderPopulars()}
        <SeeMoreTrendingGroup>
          <SeeMoreTrendingLabel>See all trending artists</SeeMoreTrendingLabel>
          <SeeAllTrendingArtistButton src={ArrowSvg} />
        </SeeMoreTrendingGroup>
      </TrendingArtistCardGroup>
    </ScrollTrendingArtistContainer>
  </TrendingSectionContainer>
);

export default TrendingSection;
