import React, { useRef, useState, useEffect } from "react";

import Banner from "assets/images/banner.png";
import Banner2 from "assets/images/banner2.jpg";

import {
  HeadlineSectionContainer,
  HeadlineSectionPicture,
  BannerContent,
  BannerLabel,
  BannerDate,
  BannerTime,
  BannerSliderContainer,
  BannerItem,
  ProgressDotContent,
  ProgressDot,
} from "./style";

const BANNERS = [
  {
    page: 1,
    backgroundImage: Banner,
    name: "YOASOBI",
    date: "Mon, Oct 12",
    time: "18.00 WIB",
  },
  {
    page: 2,
    backgroundImage: Banner2,
    name: "BTS",
    date: "Mon, Oct 12",
    time: "20.00 WIB",
  },
  {
    page: 3,
    backgroundImage: Banner,
    name: "YOASOBI",
    date: "Mon, Oct 12",
    time: "18.00 WIB",
  },
];

const renderBanners = (currentPage, bannerRef) =>
  BANNERS.map((banner) => (
    <BannerItem
      key={banner.page}
      ref={bannerRef}
      bannerRef={bannerRef}
      page={banner.page}
      currentPage={currentPage}
    >
      <HeadlineSectionPicture backgroundImage={banner.backgroundImage}>
        <BannerContent>
          <BannerLabel>{banner.name}</BannerLabel>
          <BannerDate>{banner.date}</BannerDate>
          <BannerTime>{banner.time}</BannerTime>
        </BannerContent>
      </HeadlineSectionPicture>
    </BannerItem>
  ));

const renderDots = (currentPage, setCurrentPage) =>
  BANNERS.map((banner) => (
    <ProgressDot
      key={banner.page}
      page={banner.page}
      currentPage={currentPage}
      onClick={() => setCurrentPage(banner.page)}
    />
  ));

const HeadlineSection = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const banner = useRef(null);

  useEffect(() => {
    const interval = setInterval(() => {
      if (currentPage !== BANNERS.length) {
        setCurrentPage((currentPage) => currentPage + 1);
      } else {
        setCurrentPage(1);
      }
    }, 4000);

    return () => {
      clearInterval(interval);
    };
  }, [currentPage]);

  return (
    <HeadlineSectionContainer>
      <ProgressDotContent>
        {renderDots(currentPage, setCurrentPage)}
      </ProgressDotContent>
      <BannerSliderContainer>
        {renderBanners(currentPage, banner)}
      </BannerSliderContainer>
    </HeadlineSectionContainer>
  );
};
export default HeadlineSection;
