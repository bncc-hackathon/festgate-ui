import styled from "styled-components";

import { COLOR_TEXT_SECONDARY } from "constants/color";

export const HeadlineSectionContainer = styled.div`
  font-family: Lato;
  display: flex;
  justify-content: space-evenly;
  padding: 0px 40px;
  position: relative;
  cursor: pointer;
`;

export const HeadlineSectionPicture = styled.div`
  height: 600px;
  width: 100%;
  background-image: url(${(props) => props.backgroundImage});
  background-size: cover;
  background-position: center;
  border-radius: 25px;
  display: flex;
  align-items: flex-end;
  @media (max-width: 768px) {
    height: 300px;
  }
`;

export const BannerSliderContainer = styled.div`
  overflow: hidden;
  display: flex;
  width: 100%;
`;

export const BannerItem = styled.div`
  min-width: 100%;
  transition: 1s ease;
  margin-left: ${(props) => {
    if (props.page === 1) {
      const moveOffset = props.bannerRef.current
        ? props.bannerRef.current.offsetWidth
        : 0;
      const margin = (props.currentPage - props.page) * -moveOffset;
      return margin !== 0
        ? margin - (props.currentPage - props.page) * 40
        : margin;
    } else {
      return 0;
    }
  }}px;
  margin-right: 40px;
`;

export const BannerContent = styled.div`
  padding: 40px 40px;
  height: 160px;
  border-radius: 0 0 25px 25px;
  flex: 1;
  background: linear-gradient(
    0deg,
    rgba(252, 70, 107, 0.62) 11.14%,
    rgba(75, 80, 94, 0) 104.76%
  );
`;

export const ProgressDotContent = styled.div`
  position: absolute;
  bottom: 20px;
  display: flex;
`;

export const ProgressDot = styled.div`
  background-color: ${(props) =>
    props.currentPage === props.page ? "white" : COLOR_TEXT_SECONDARY};
  width: 20px;
  height: 20px;
  border-radius: 50%;
  margin: 0px 8px;
  cursor: pointer;
  @media (max-width: 768px) {
    width: 10px;
    height: 10px;
  }
`;

export const BannerLabel = styled.h1`
  color: white;
  letter-spacing: 0.08em;
`;

export const BannerDate = styled.h2`
  color: white;
  letter-spacing: 0.08em;
  margin: 0px;
`;

export const BannerTime = styled.h2`
  color: white;
  letter-spacing: 0.08em;
  margin: 0px;
`;
