import React from "react";

import ArrowSvg from "assets/images/Arrow.svg";

import {
  PopularSectionContainer,
  PopularSectionHeadline,
  ScrollPopularArtistContainer,
  PopularArtistCardGroup,
  PopularArtistCard,
  PopularArtistName,
  SeeMorePopularGroup,
  SeeMorePopularLabel,
  SeeAllPopularArtistButton,
} from "./style";

const NAMES = ["BTS", "BLACKPINK", "TWICE", "YOASOBI"];

const renderPopulars = () =>
  NAMES.map((name) => (
    <PopularArtistCard key={name}>
      <PopularArtistName>{name}</PopularArtistName>
    </PopularArtistCard>
  ));

const PopularSection = () => (
  <PopularSectionContainer>
    <PopularSectionHeadline>Popular Artist</PopularSectionHeadline>
    <ScrollPopularArtistContainer>
      <PopularArtistCardGroup>
        {renderPopulars()}
        <SeeMorePopularGroup>
          <SeeMorePopularLabel>See all popular artists</SeeMorePopularLabel>
          <SeeAllPopularArtistButton src={ArrowSvg} />
        </SeeMorePopularGroup>
      </PopularArtistCardGroup>
    </ScrollPopularArtistContainer>
  </PopularSectionContainer>
);

export default PopularSection;
