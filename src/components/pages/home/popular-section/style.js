import styled from "styled-components";

import { COLOR_TEXT_SECONDARY, COLOR_TEXT_MAIN } from "constants/color";

export const PopularSectionContainer = styled.div`
  font-family: Lato;
  padding: 40px;
  background-size: cover;
`;
export const PopularSectionHeadline = styled.h1`
  color: ${COLOR_TEXT_MAIN};
  margin-top: 0px;
`;

export const ScrollPopularArtistContainer = styled.div`
  overflow: scroll;
`;

export const PopularArtistCardGroup = styled.div`
  display: flex;
`;

export const PopularArtistCard = styled.div`
  border-radius: 10px;
  display: flex;
  align-items: flex-end;
  background-color: ${COLOR_TEXT_MAIN};
  color: ${COLOR_TEXT_SECONDARY};
  min-width: 200px;
  height: 200px;
  margin-right: 40px;
  padding: 20px;
`;

export const PopularArtistName = styled.h2``;

export const SeeMorePopularGroup = styled.div`
  width: 90px;
  height: 240px;
  display: flex;
  align-items: flex-end;
  cursor: pointer;
`;
export const SeeMorePopularLabel = styled.h1`
  color: ${COLOR_TEXT_MAIN};
  margin: 0;
`;

export const SeeAllPopularArtistButton = styled.img`
  margin-left: 30px;
`;
