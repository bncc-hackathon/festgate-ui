import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { COLOR_SECONDARY, COLOR_TEXT_MAIN } from "constants/color";

export const UpcomingSectionContainer = styled.div`
  font-family: Lato;
  padding: 40px 0px 40px 40px;
`;
export const UpcomingSectionHeadline = styled.h1`
  color: ${COLOR_TEXT_MAIN};
  margin-bottom: 0px;
  letter-spacing: 0.08em;
`;

export const ScrollUpcomingConcertContainer = styled.div`
  overflow: scroll;
`;

export const UpcomingConcertCardGroup = styled.div`
  display: flex;
  overflow: scroll;
  width: 100vw;
  flex: 1;
  margin-top: 20px;
`;

export const UpcomingConcertCard = styled.div`
  flex: 1;
  background-image: url(${(props) => props.backgroundImage});
  min-width: 350px;
  height: 550px;
  position: relative;
  background-position: center;
  background-size: cover;
  border-radius: 10px;
  margin-right: 40px;
  cursor: pointer;
  @media (max-width: 768px) {
    height: 400px;
    min-width: 250px;
  }
`;

export const UpcomingConcertCardContent = styled.div`
  padding: 0px 20px 20px 20px;
  font-family: Lato;
  background-color: #8056ca;
  color: white;
  position: absolute;
  bottom: 0;
  width: 310px;
  height: 120px;
  border-radius: 0px 0px 10px 10px;
  @media (max-width: 768px) {
    height: 100px;
    width: 210px;
  }
`;

export const BlurryCardContent = styled.div`
  padding: 20px;
  position: absolute;
  height: 100px;
  width: 310px;
  bottom: 140px;
  background: linear-gradient(360deg, #8056ca 0%, rgba(75, 80, 94, 0) 100%);
  @media (max-width: 768px) {
    bottom: 120px;
    width: 210px;
  }
`;

export const ConcertName = styled.h1`
  margin-bottom: 10px;
  @media (max-width: 768px) {
    font-size: 1.5em;
  }
`;

export const ConcertDate = styled.h4`
  margin: 0px;
`;
export const ConcertTime = styled.h4`
  margin-top: 8px;
`;

export const DetailButtonSection = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-bottom: 20px;
`;

export const DetailButton = withStyles({
  root: {
    fontFamily: "Lato",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_SECONDARY,
    color: "white",
    padding: "2px 30px",
    borderRadius: "25px",
  },
})(Button);
