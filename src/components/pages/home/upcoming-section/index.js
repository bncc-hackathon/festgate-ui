import React from "react";
import { useHistory } from "react-router-dom";

import Yoasobi from "assets/images/Yoasobi.png";
import BlackPink from "assets/images/BlackPink.png";
import Boc from "assets/images/boc.png";

import {
  UpcomingConcertCard,
  UpcomingSectionContainer,
  UpcomingSectionHeadline,
  UpcomingConcertCardContent,
  UpcomingConcertCardGroup,
  BlurryCardContent,
  ConcertName,
  ConcertDate,
  ConcertTime,
  ScrollUpcomingConcertContainer,
} from "./style";

const CONCERTS = [
  {
    posters: Yoasobi,
    name: "YOASOBI",
    date: "Mon, Oct 12",
    time: "18.00 WIB",
  },
  {
    posters: BlackPink,
    name: "BlackPink",
    date: "Mon, Oct 12",
    time: "19.00 WIB",
  },
  {
    posters: Yoasobi,
    name: "BTS",
    date: "Mon, Oct 12",
    time: "20.00 WIB",
  },
  {
    posters: Boc,
    name: "Bump Of Chicken",
    date: "Mon, Oct 12",
    time: "21.00 WIB",
  },
  {
    posters: Yoasobi,
    name: "LISA",
    date: "Tue, Oct 13",
    time: "18.00 WIB",
  },
  {
    posters: Yoasobi,
    name: "Twice",
    date: "Wed, Oct 14",
    time: "18.00 WIB",
  },
];

const renderConcerts = (handlingCardClick) =>
  CONCERTS.map((concert) => (
    <UpcomingConcertCard
      onClick={handlingCardClick}
      key={concert.name}
      backgroundImage={concert.posters}
    >
      <BlurryCardContent />
      <UpcomingConcertCardContent>
        <ConcertName>{concert.name}</ConcertName>
        <ConcertDate>{concert.date}</ConcertDate>
        <ConcertTime>{concert.time}</ConcertTime>
      </UpcomingConcertCardContent>
    </UpcomingConcertCard>
  ));

const UpcomingSection = () => {
  const history = useHistory();

  const handlingCardClick = () => {
    history.push("/checkout");
  };

  return (
    <UpcomingSectionContainer>
      <UpcomingSectionHeadline>Upcoming Concerts</UpcomingSectionHeadline>
      <ScrollUpcomingConcertContainer>
        <UpcomingConcertCardGroup>
          {renderConcerts(handlingCardClick)}
        </UpcomingConcertCardGroup>
      </ScrollUpcomingConcertContainer>
    </UpcomingSectionContainer>
  );
};

export default UpcomingSection;
