import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import Banner from "assets/images/banner.png";

import { COLOR_TEXT_SECONDARY, COLOR_MAIN } from "constants/color";

export const CheckoutCardSectionContainer = styled.div`
  background-color: white;
  font-family: Lato;
  transition: 0.5s ease;
  width: ${(props) => (props.currentPage !== 4 ? 350 : 840)}px;
  min-height: ${(props) => props.currentPage === 4 && 580}px;
  box-shadow: 0px 4px 10px 4px rgba(105, 105, 105, 0.25);
  border-radius: 10px;
  letter-spacing: 0.08em;
  @media (max-width: 1280px) {
    width: ${(props) => (props.currentPage !== 4 ? 350 : 480)}px;
    min-height: 20%;
  }
`;

export const CheckoutCardHeadline = styled.div`
  height: 130px;
  width: 100%;
  background: url(${Banner});
  border-radius: 10px 10px 0px 0px;
`;

export const CheckoutCardContent = styled.div`
  padding: 20px;
`;

export const TicketNameLabel = styled.h4`
  margin: 0;
  color: ${COLOR_TEXT_SECONDARY};
`;

export const TicketConcertName = styled.h4`
  margin: 8px 0;
  font-weight: bolder;
`;

export const TicketConcertDate = styled.h5`
  margin: 8px 0;
`;

export const TicketConcertTime = styled.h5`
  margin: 8px 0;
`;

export const LineSeparator = styled.hr`
  margin: 16px 0px;
`;

export const PriceGroup = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const PriceLabel = styled.h4`
  margin-top: 0px;
  color: ${COLOR_TEXT_SECONDARY};
  margin-bottom: 12px;
`;

export const PriceValue = styled.h4`
  margin-top: 0px;
  margin-bottom: 12px;
`;

export const TotalPriceLabel = styled.h4`
  margin: 0px;
  font-weight: bolder;
`;
export const TotalPriceValue = styled.h4`
  margin: 0px;
  font-weight: bolder;
  color: ${COLOR_MAIN};
`;

export const CarouselGroup = styled.div`
  overflow: hidden;
  display: flex;
  max-height: 550px;
`;

export const CarouselItem = styled.div`
  min-width: 100%;
  transition: 0.5s ease;
  margin-left: ${(props) => {
    if (props.page === 1) {
      return (props.currentPage - props.page) * -350;
    } else {
      return 0;
    }
  }}px;
`;

export const CheckoutButton = withStyles({
  root: {
    fontFamily: "Lato",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_MAIN,
    color: "white",
    padding: "8px 40px",
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
  },
})(Button);
