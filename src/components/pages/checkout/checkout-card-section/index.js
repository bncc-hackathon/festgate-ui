import React from "react";
import CheckoutOverview from "components/pages/checkout/checkout-overview";
import CheckoutPayment from "components/pages/checkout/checkout-payment";
import CreditSection from "components/pages/checkout/credit-section";

import {
  CheckoutCardSectionContainer,
  CarouselGroup,
  CarouselItem,
} from "./style";
import CameraSection from "../camera-section";

const CheckoutCardSection = ({ currentPage, setCurrentPage }) => (
  <CheckoutCardSectionContainer currentPage={currentPage}>
    <CarouselGroup>
      {currentPage !== 4 && (
        <>
          <CarouselItem page={1} currentPage={currentPage}>
            <CheckoutOverview handleChangePage={setCurrentPage} />
          </CarouselItem>
          <CarouselItem>
            <CheckoutPayment handleChangePage={setCurrentPage} />
          </CarouselItem>
          <CarouselItem>
            <CreditSection handleChangePage={setCurrentPage} />
          </CarouselItem>
        </>
      )}
      {currentPage === 4 && (
        <CarouselItem>
          <CameraSection
            handleChangePage={setCurrentPage}
            currentPage={currentPage}
          />
        </CarouselItem>
      )}
    </CarouselGroup>
  </CheckoutCardSectionContainer>
);

export default CheckoutCardSection;
