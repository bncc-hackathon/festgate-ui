import React from "react";
import { useHistory } from "react-router-dom";
import {
  SuccessSectionContainer,
  SuccessSectionDescription,
  SuccessSectionLabel,
  TicketIcon,
  SeeTicketDetailButton,
  FindOtherConcertButton,
  ButtonGroup,
} from "./style";

import Icon from "assets/images/ticket.svg";

const SuccessSection = () => {
  const history = useHistory();
  return (
    <SuccessSectionContainer>
      <TicketIcon src={Icon} />
      <SuccessSectionLabel>Payment Successful!</SuccessSectionLabel>
      <SuccessSectionDescription>
        Your booking for “YOASOBI Concert” has been successfully made
      </SuccessSectionDescription>
      <ButtonGroup>
        <SeeTicketDetailButton onClick={() => history.push("/")}>
          See Ticket Details
        </SeeTicketDetailButton>
        <FindOtherConcertButton onClick={() => history.push("/")}>
          See Ticket Details
        </FindOtherConcertButton>
      </ButtonGroup>
    </SuccessSectionContainer>
  );
};

export default SuccessSection;
