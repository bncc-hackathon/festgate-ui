import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { COLOR_MAIN } from "constants/color";

export const SuccessSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const TicketIcon = styled.img``;

export const SuccessSectionLabel = styled.h1``;

export const SuccessSectionDescription = styled.h2`
  width: 60%;
  text-align: center;
`;

export const ButtonGroup = styled.div`
  margin: 40px 0px;
  width: 40%;
`;

export const SeeTicketDetailButton = withStyles({
  root: {
    fontFamily: "Lato",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_MAIN,
    color: "white",
    padding: "8px 40px",
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
  },
})(Button);

export const FindOtherConcertButton = withStyles({
  root: {
    margin: "20px 0px",
    fontFamily: "Lato",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: "white",
    color: COLOR_MAIN,
    padding: "8px 40px",
    borderRadius: "25px",
    borderColor: COLOR_MAIN,
    borderStyle: "solid",
    borderWidth: "1px",
    width: "100%",
    "&:hover": {
      backgroundColor: "white",
    },
  },
})(Button);
