import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import { COLOR_MAIN, COLOR_SECONDARY } from "constants/color";

export const CameraSectionContainer = styled.div`
  padding: 20px;
  position: relative;
`;

export const FrameContainer = styled.div`
  position: absolute;
  left: 260px;
  top: 100px;
  @media (max-width: 1280px) {
    left: 180px;
  }
`;

export const FocusFrame = styled.img`
  width: 300px;
  height: 360px;
  @media (max-width: 1280px) {
    width: ${(props) => {
      return props.currentPage !== 4 ? 350 : 120;
    }}px;
  }
`;

export const ConfirmButton = withStyles({
  root: {
    fontFamily: "Lato",
    marginTop: "10px",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_MAIN,
    color: "white",
    padding: "8px 40px",
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
  },
})(Button);

export const ErrorButton = withStyles({
  root: {
    fontFamily: "Lato",
    marginTop: "10px",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_SECONDARY,
    color: "white",
    padding: "8px 40px",
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_SECONDARY,
    },
  },
})(Button);
