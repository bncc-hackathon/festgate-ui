import React, { useState } from "react";
import Webcam from "react-webcam";
import Frame from "assets/images/frame.svg";
import imageUtil from "utils/image";
import {
  CameraSectionContainer,
  FocusFrame,
  FrameContainer,
  ConfirmButton,
  ErrorButton,
} from "./style";
import { useRef } from "react";
import { useDispatch } from "react-redux";

import { verify } from "store/user/actions";

const CameraSection = ({ currentPage, handleChangePage }) => {
  const cameraSectionContainerRef = useRef(null);
  const [error, setError] = useState(false);
  const webcamRef = React.useRef(null);
  const dispatch = useDispatch();
  let images = [];

  const pushImage = React.useCallback(
    (imageSrc) => {
      const promise = imageUtil.base64ToBlob(imageSrc);
      promise.then((res) => {
        const image = imageUtil.blobToFile(res);
        images.push(image);
        if (images.length !== 5) {
          setTimeout(() => {
            const newImage = webcamRef.current.getScreenshot();
            pushImage(newImage);
          }, 500);
        } else {
          dispatch(
            verify(images, {
              success: () => handleChangePage(5),
              failed: () => setError(true),
            })
          );
        }
      });
    },
    [handleChangePage, images, dispatch]
  );

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    pushImage(imageSrc);
  }, [webcamRef, pushImage]);

  return (
    <CameraSectionContainer ref={cameraSectionContainerRef}>
      {currentPage === 4 ? (
        <>
          <Webcam ref={webcamRef} style={{ width: "100%" }} />
          <FrameContainer>
            <FocusFrame
              currentPage={currentPage}
              containerRef={cameraSectionContainerRef}
              src={Frame}
            />
            {error ? (
              <ErrorButton onClick={capture}>Retry</ErrorButton>
            ) : (
              <ConfirmButton onClick={capture}>Confirm</ConfirmButton>
            )}
          </FrameContainer>
        </>
      ) : (
        <div></div>
      )}
    </CameraSectionContainer>
  );
};

export default CameraSection;
