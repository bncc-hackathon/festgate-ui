import React, { useState } from "react";
import CC from "assets/images/cc.png";
import {
  CreditSectionContainer,
  PaymentIcon,
  PaymentLabel,
  PaymentIconLabel,
  LineSeparator,
  InputTextField,
  ErrorLabel,
  CheckoutButton,
  TotalPriceValue,
  PriceGroup,
  TotalPriceLabel,
  CreditCardGroup,
  CreditCardExpiredGroup,
  CreditCardExpiredYear,
  CreditCardExpiredMonth,
  CreditCardCvvGroup,
  CreditCardCvv,
  CreditCardSeparator,
  CreditCardExpiredDateGroup,
  CreditCardCvvLabel,
  CreditCardExpiredDateLabel,
} from "./style";

const FORM_LIST = {
  name: {
    placeholder: "Name",
    value: "",
    error: "",
  },
  number: {
    placeholder: "Card Number",
    value: "",
    error: "",
  },
  month: {
    value: "",
    error: "",
  },
  year: {
    value: "",
    error: "",
  },
  cvv: {
    value: "",
    error: "",
  },
};

const CreditSection = ({ handleChangePage }) => {
  const [form, setForm] = useState(FORM_LIST);

  const handleChange = (e) => {
    let updatedForm = { ...form };
    updatedForm[e.target.name].value = e.target.value;
    updatedForm[e.target.name].error = "";
    setForm(updatedForm);
  };

  const handleSubmit = (e) => {
    const isValid = isValidateForm();
    if (isValid) {
      handleChangePage(4);
    }
  };

  const isValidateForm = () => {
    let isValid = true;
    let updatedForm = { ...form };
    Object.keys(FORM_LIST).forEach((input) => {
      if (form[input].value === "") {
        updatedForm[
          input
        ].error = `${FORM_LIST[input].placeholder} can't be null`;
        setForm(updatedForm);
        isValid = false;
      }
    });
    return isValid;
  };

  return (
    <CreditSectionContainer>
      <PaymentIconLabel>
        <PaymentIcon src={CC} />
        <PaymentLabel>Credit Cards</PaymentLabel>
      </PaymentIconLabel>
      <LineSeparator />
      <InputTextField
        onChange={handleChange}
        name="name"
        type="text"
        label="Name on Card"
        variant="outlined"
      />
      {form["name"].error && <ErrorLabel>{form["name"].error}</ErrorLabel>}
      <InputTextField
        onChange={handleChange}
        name="number"
        type="text"
        label="Card Number"
        variant="outlined"
      />
      {form["number"].error && <ErrorLabel>{form["number"].error}</ErrorLabel>}
      <CreditCardGroup>
        <CreditCardExpiredGroup>
          <CreditCardExpiredDateLabel>
            Expiration Date
          </CreditCardExpiredDateLabel>
          <CreditCardExpiredDateGroup>
            <CreditCardExpiredMonth
              placeholder="MM"
              variant="outlined"
              onChange={handleChange}
              name="month"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <CreditCardSeparator> /</CreditCardSeparator>
            <CreditCardExpiredYear
              placeholder="YY"
              variant="outlined"
              name="year"
              onChange={handleChange}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </CreditCardExpiredDateGroup>
        </CreditCardExpiredGroup>
        <CreditCardCvvGroup>
          <CreditCardCvvLabel>CVV</CreditCardCvvLabel>
          <CreditCardCvv
            placeholder="CVV"
            variant="outlined"
            onChange={handleChange}
            name="cvv"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </CreditCardCvvGroup>
      </CreditCardGroup>
      <LineSeparator />
      <PriceGroup>
        <TotalPriceLabel>Total Payment</TotalPriceLabel>
        <TotalPriceValue>Rp 755.000</TotalPriceValue>
      </PriceGroup>
      <LineSeparator />
      <CheckoutButton onClick={handleSubmit}>Confirm Button</CheckoutButton>
    </CreditSectionContainer>
  );
};

export default CreditSection;
