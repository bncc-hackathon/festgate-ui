import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { TextField, Button } from "@material-ui/core";
import { COLOR_TEXT_DANGER, COLOR_MAIN } from "constants/color";

export const CreditSectionContainer = styled.div`
  padding: 20px;
`;

export const PaymentLabel = styled.label`
  margin-left: 10px;
`;

export const PaymentIcon = styled.img`
  height: 16px;
`;

export const PaymentIconLabel = styled.div`
  display: flex;
`;

export const LineSeparator = styled.hr`
  margin: 16px 0px;
`;

export const ErrorLabel = styled.label`
  font-family: Lato
  color: ${COLOR_TEXT_DANGER};
`;

export const PriceGroup = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const TotalPriceLabel = styled.h4`
  margin: 0px;
  font-weight: bolder;
`;
export const TotalPriceValue = styled.h4`
  margin: 0px;
  font-weight: bolder;
  color: ${COLOR_MAIN};
`;

export const CreditCardGroup = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 12px 0px;
  margin-bottom: 24px;
`;

export const CreditCardExpiredGroup = styled.div`
  flex: 4;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

export const CreditCardExpiredYear = withStyles({
  root: {
    outline: "none",
    textDecoration: "none",
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(TextField);

export const CreditCardSeparator = styled.label`
  margin: 0px 20px;
`;

export const CreditCardExpiredDateGroup = styled.div`
  display: flex;
  align-items: center;
`;
export const CreditCardExpiredDateLabel = styled.label`
  margin-bottom: 12px;
`;

export const CreditCardExpiredMonth = withStyles({
  root: {
    outline: "none",
    textDecoration: "none",
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(TextField);

export const CreditCardCvvGroup = styled.div`
  display: flex;
  margin-left: 30px;
  flex-direction: column;
  justify-content: center;
  flex: 2;
`;

export const CreditCardCvvLabel = styled.label`
  margin-bottom: 12px;
`;

export const CreditCardCvv = withStyles({
  root: {
    outline: "none",
    textDecoration: "none",
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(TextField);

export const InputTextField = withStyles({
  root: {
    margin: "12px 0px",
    width: "100%",
    outline: "none",
    textDecoration: "none",
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderRadius: `10px`,
      },
    },
  },
})(TextField);

export const CheckoutButton = withStyles({
  root: {
    fontFamily: "Lato",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_MAIN,
    color: "white",
    padding: "8px 40px",
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
  },
})(Button);
