import React from "react";

import {
  PageNumberSectionContainer,
  CheckoutStepProgress,
  CheckoutProgressSection,
  CheckoutStepNumber,
  CheckoutStepLabel,
  CheckoutLabelSection,
} from "./style";

const PageNumberSection = ({ currentPage }) => (
  <PageNumberSectionContainer>
    <CheckoutProgressSection>
      <CheckoutStepNumber isActive={1 - currentPage < 1}>1</CheckoutStepNumber>
      <CheckoutStepProgress isActive={2 - currentPage < 1} />
      <CheckoutStepNumber isActive={2 - currentPage < 1}>2</CheckoutStepNumber>
    </CheckoutProgressSection>
    <CheckoutLabelSection>
      <CheckoutStepLabel isActive={1 - currentPage < 1}>
        Checkout
      </CheckoutStepLabel>
      <CheckoutStepLabel isActive={2 - currentPage < 1}>
        Payment <br />
        Method
      </CheckoutStepLabel>
    </CheckoutLabelSection>
  </PageNumberSectionContainer>
);

export default PageNumberSection;
