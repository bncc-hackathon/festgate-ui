import styled from "styled-components";

import {
  COLOR_SECONDARY,
  COLOR_TEXT_MAIN,
  COLOR_TEXT_SECONDARY,
} from "constants/color";

export const PageNumberSectionContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin: 20px 0px;
`;

export const CheckoutStepGroup = styled.div`
  display: flex;
  flex-direction: column;
`;

export const CheckoutStepNumber = styled.div`
  font-family: Lato;
  transition-delay: 0.5s;
  transition: background-color 0.5s linear;
  background-color: ${(props) =>
    props.isActive ? COLOR_SECONDARY : COLOR_TEXT_SECONDARY};
  color: white;
  padding: 20px;
  border-radius: 50%;
  width: 12px;
  height: 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px 20px;
`;

export const CheckoutStepLabel = styled.div`
  font-family: Lato;
  transition-delay: 0.5s;
  transition: 0.5s ease;
  color: ${(props) =>
    props.isActive ? COLOR_TEXT_MAIN : COLOR_TEXT_SECONDARY};
  text-align: center;
  margin: 0px 32px;
  font-weight: bolder;
`;

export const CheckoutStepProgress = styled.div`
  width: 40px;
  height: 3px;
  transition: background-color 0.5s linear;
  background-color: ${(props) =>
    props.isActive ? COLOR_SECONDARY : COLOR_TEXT_SECONDARY};
`;

export const CheckoutProgressSection = styled.div`
  display: flex;
  align-items: center;
`;

export const CheckoutLabelSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  width: 100%;
`;
