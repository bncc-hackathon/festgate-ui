import React from "react";

import {
  CheckoutCardHeadline,
  CheckoutCardContent,
  TicketNameLabel,
  TicketConcertName,
  TicketConcertDate,
  TicketConcertTime,
  LineSeparator,
  PriceGroup,
  PriceLabel,
  PriceValue,
  TotalPriceLabel,
  TotalPriceValue,
  CheckoutButton,
} from "./style";

const CheckoutOverview = ({ handleChangePage }) => {
  const handleSubmitOverview = () => {
    handleChangePage(2);
  };

  return (
    <>
      <CheckoutCardHeadline />
      <CheckoutCardContent>
        <TicketNameLabel>Ticket Name</TicketNameLabel>
        <TicketConcertName>YOASOBI Concert Ticket</TicketConcertName>
        <TicketConcertDate>Monday, October 12 2020</TicketConcertDate>
        <TicketConcertTime>18.00 WIB</TicketConcertTime>
        <LineSeparator />
        <PriceGroup>
          <PriceLabel>Ticket Price</PriceLabel>
          <PriceValue>Rp 750.000</PriceValue>
        </PriceGroup>
        <PriceGroup>
          <PriceLabel>Quantity</PriceLabel>
          <PriceValue>x 1</PriceValue>
        </PriceGroup>
        <PriceGroup>
          <PriceLabel>Total Price</PriceLabel>
          <PriceValue>Rp 750.000</PriceValue>
        </PriceGroup>
        <PriceGroup>
          <PriceLabel>Admin Fee</PriceLabel>
          <PriceValue>Rp 5.000</PriceValue>
        </PriceGroup>
        <LineSeparator />
        <PriceGroup>
          <TotalPriceLabel>Total Payment</TotalPriceLabel>
          <TotalPriceValue>Rp 755.000</TotalPriceValue>
        </PriceGroup>
        <LineSeparator />
        <CheckoutButton onClick={handleSubmitOverview}>
          Confirm Button
        </CheckoutButton>
      </CheckoutCardContent>
    </>
  );
};

export default CheckoutOverview;
