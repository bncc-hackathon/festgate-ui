import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

import Banner from "assets/images/banner.png";

import { COLOR_TEXT_SECONDARY, COLOR_MAIN } from "constants/color";

export const CheckoutCardHeadline = styled.div`
  height: 130px;
  width: 100%;
  background: url(${Banner});
  border-radius: 10px 10px 0px 0px;
  background-size: cover;
`;

export const CheckoutCardContent = styled.div`
  padding: 20px;
`;

export const TicketNameLabel = styled.h4`
  margin: 0;
  color: ${COLOR_TEXT_SECONDARY};
`;

export const TicketConcertName = styled.h4`
  margin: 8px 0;
  font-weight: bolder;
`;

export const TicketConcertDate = styled.h5`
  margin: 8px 0;
`;

export const TicketConcertTime = styled.h5`
  margin: 8px 0;
`;

export const LineSeparator = styled.hr`
  margin: 16px 0px;
`;

export const PriceGroup = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const PriceLabel = styled.h4`
  margin-top: 0px;
  color: ${COLOR_TEXT_SECONDARY};
  margin-bottom: 12px;
`;

export const PriceValue = styled.h4`
  margin-top: 0px;
  margin-bottom: 12px;
`;

export const TotalPriceLabel = styled.h4`
  margin: 0px;
  font-weight: bolder;
`;
export const TotalPriceValue = styled.h4`
  margin: 0px;
  font-weight: bolder;
  color: ${COLOR_MAIN};
`;

export const CheckoutButton = withStyles({
  root: {
    fontFamily: "Lato",
    display: "flex",
    justifyContent: "center",
    textTransform: "none",
    backgroundColor: COLOR_MAIN,
    color: "white",
    padding: "8px 40px",
    borderRadius: "25px",
    width: "100%",
    "&:hover": {
      backgroundColor: COLOR_MAIN,
    },
  },
})(Button);
