import styled from "styled-components";

import { COLOR_MAIN } from "constants/color";

export const CheckoutPaymentContainer = styled.div`
  padding: 20px;
`;

export const PaymentType = styled.h3``;

export const PaymentLabel = styled.label`
  margin-left: 10px;
`;

export const PaymentBox = styled.div`
  display: flex;
  background: #ffffff;
  border: 1px solid #e6e6e6;
  box-sizing: border-box;
  border-radius: 8px;
  padding: 20px;
  justify-content: space-between;
  margin-bottom: 20px;
  align-items: center;
`;

export const PaymentIcon = styled.img`
  height: 16px;
`;

export const PaymentIconLabel = styled.div`
  display: flex;
`;

export const PriceGroup = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const TotalPriceLabel = styled.h4`
  margin: 0px;
  font-weight: bolder;
`;
export const TotalPriceValue = styled.h4`
  margin: 0px;
  font-weight: bolder;
  color: ${COLOR_MAIN};
`;
