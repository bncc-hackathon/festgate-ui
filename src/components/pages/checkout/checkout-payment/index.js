import React from "react";

import ArrowForward from "@material-ui/icons/ArrowForwardIos";

import CC from "assets/images/cc.png";
import Gopay from "assets/images/gopay.png";
import Bca from "assets/images/bca.png";
import Bni from "assets/images/bni.png";
import {
  CheckoutPaymentContainer,
  PaymentType,
  PaymentLabel,
  PaymentBox,
  PaymentIcon,
  PaymentIconLabel,
  TotalPriceValue,
  PriceGroup,
  TotalPriceLabel,
} from "./style";

const PAYMENTS = [
  {
    type: "Debit Card",
    methods: [
      {
        label: "Credit Cards",
        icon: CC,
      },
    ],
  },
  {
    type: "Instant Payment",
    methods: [
      {
        label: "",
        icon: Gopay,
      },
    ],
  },
  {
    type: "Virtual Account",
    methods: [
      {
        label: "",
        icon: Bca,
      },
      {
        label: "",
        icon: Bni,
      },
    ],
  },
];

const renderMethods = (methods, handleChangePage) =>
  methods.map((method) => (
    <PaymentBox onClick={() => handleChangePage(3)}>
      {method.label !== "" ? (
        <PaymentIconLabel>
          <PaymentIcon src={method.icon} />
          <PaymentLabel>{method.label}</PaymentLabel>
        </PaymentIconLabel>
      ) : (
        <PaymentIcon src={method.icon} />
      )}
      <ArrowForward />
    </PaymentBox>
  ));

const renderPayments = (handleChangePage) =>
  PAYMENTS.map((payment) => (
    <>
      <PaymentType>{payment.type}</PaymentType>
      {renderMethods(payment.methods, handleChangePage)}
    </>
  ));

const CheckoutPayment = ({ handleChangePage }) => (
  <CheckoutPaymentContainer>
    {renderPayments(handleChangePage)}
    <PriceGroup>
      <TotalPriceLabel>Total Payment</TotalPriceLabel>
      <TotalPriceValue>Rp 755.000</TotalPriceValue>
    </PriceGroup>
  </CheckoutPaymentContainer>
);

export default CheckoutPayment;
