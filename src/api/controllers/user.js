import httpHandler from "utils/http-handler";
import config from "config";

const urls = config.urls.user;

const ApiToken = "c3284040-0c1a-11eb-bbee-0a05adc1166c";

export default {
  getActiveUser() {
    const userToken = localStorage.getItem("userToken");
    const userId = localStorage.getItem("userId");
    const data = httpHandler.getRequest(
      urls.getActiveUser(userId),
      {},
      {
        "Bearer-Token": userToken,
        "GAuth-Token": ApiToken,
      }
    );
    return data;
  },
  login(body) {
    const data = httpHandler.postRequest(urls.login, body, {
      "GAuth-Token": ApiToken,
    });
    return data;
  },
  register(body) {
    const data = httpHandler.postRequest(urls.register, body, {
      "GAuth-Token": ApiToken,
    });
    return data;
  },
  uploadToS3(file) {
    let fd = new FormData();
    fd.append("file", file);
    const data = httpHandler.postRequest(urls.uploadToS3, fd, {
      "GAuth-Token": ApiToken,
      "Content-Type": "multipart/form-data",
    });
    return data;
  },
  verify(request) {
    let fd = new FormData();
    const userToken = localStorage.getItem("userToken");
    const userId = localStorage.getItem("userId");
    fd.append("user_id", userId);
    fd.append("files", request);
    const data = httpHandler.postRequest(urls.compareFace(userId), fd, {
      "GAuth-Token": ApiToken,
      "Bearer-Token": userToken,
      "Content-Type": "multipart/form-data",
    });
    return data;
  },
};
