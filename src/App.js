import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { fetchActiveUser } from "store/user/actions";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Home from "pages/home";
import ProtectedRoute from "./components/common/protected-route";
import Register from "./pages/register";
import Login from "./pages/login";
import Checkout from "./pages/checkout";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchActiveUser());
  }, [dispatch]);
  return (
    <BrowserRouter>
      <Switch>
        <ProtectedRoute path="/protected" component={Home} />
        <Route path="/checkout" component={Checkout} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="" component={Home} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
